# AEP project Axe 1

Ce document décrit les travaux réalisés pour l'axe ** du project AEP \[...\]

L'organisation de l'exécution de se projet s'est déroulé comme suit, sur un axe temporel:

1. Analyse des besoins/ Mise en place d'un formalisme, modélisation
2. Stockage des données, visualisation, cas d'utilisation "simple"
3. Utilisation des données, cas d'utilisation "avancée"

Bien que ces étapes suivent un ordre chronologique, l'évolution de l'une peut impliquer des changements sur les autres, au moins pendant la création d'un (ou plusieurs) prototypes.

## Phase 1: 

Lors de cette étape, il a fallu établir une communication entre les agronomes et les informaticiens, ou encore entre la couche métier et la couche technique. En fonction de papiers existant (lien vers l'article de Wery) et du choix d'une approche [mécaniste](https://fr.wikipedia.org/wiki/M%C3%A9canisme_(philosophie))pour décrire les phénomènes décrits, nous sommes arrivés à la construction d'un modèle entre *Processus* et *Agent*.

### Travaux réalisés

Liens vers les diapos

## Phase 2:

Choix d'un support de stockage adapté (base de données) avec neo4j et le langage de requête cypher. Remplissage et exploitation 'basique' de la base de connaissance.

### Travaux réalisé

Ouverture d'un serveur accessible depuis internet. Déploiement d'une base de données neo4j et Virtuoso. Premier prototype d'un client web pour l'aquisition de donnée. Prototype actuel d'un outil d'aquisition et de partage de données systémique. Package python facilitant le requêtage, répondant à une liste de cas d'utilisation basique.

## Phase 3

Exploitation avancé de l'outil, recherche et développement de nouvelles fonctionalités : temporalité des processus, mise en contexte, conditionement, connection avec d'autre formalisme "proche" (GECO)... Il s'agit d'autant d'objectif de développement à court et moyen terme.

### Travaux réalisé

 - ETL de chargement de GECO
 - Temporalité (travail avec le lirmm, approche parcours de graphe)
 - Condition nécessaire / émergence d'un agent
 - Mise en contexte géographique