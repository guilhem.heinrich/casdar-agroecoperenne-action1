#! /bin/bash


docker run \
    --rm \
    --publish=7474:7474 --publish=7687:7687 \
    -v $HOME/neo4j/data:/data \
    -v $HOME/neo4j/conf:/conf \
    -v $HOME/neo4j/logs:/logs \
    -v $HOME/neo4j/import:/var/lib/neo4j/import \
    --name neo4j \
    neo4j:3.5