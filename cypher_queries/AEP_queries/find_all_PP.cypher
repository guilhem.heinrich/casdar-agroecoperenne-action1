// Afficher toutes les plantes principales
MATCH (n:LS_Agent_PP) RETURN *

// Afficher tous les contextes temporelles (périodes)
MATCH (n:LS_ctx_temp) RETURN *

// Afficher toutes les plantes principales et leur parties (jusqu'à une prof)
MATCH (n:LS_Agent_PP)
OPTIONAL MATCH (n)-[:part_of*0..2]- (p) 
RETURN *

// Afficher toutes les plantes principales et leur parties (jusqu'à une prof)
// et les processus qui sont défavorables aux PP
MATCH (pp:LS_Agent_PP)
OPTIONAL MATCH (pp)-[:part_of*0..2]- (part) <-[:involve {role: "defavorable"}]- (pro_def:LS_Processus) 
RETURN *

MATCH (pp:LS_Agent_PP) -[:part_of*0..2]- (part) <-[:involve {role: "defavorable"}]- (pro_def:LS_Processus)
MATCH (pro_def) -[:involve]-> (bioagressor:LS_Agent) 
RETURN *


// High level agent
MATCH (id) 
WHERE NOT id:GECO AND labels(id) IN ['LS_Agent', 'LS_Agent_PP']
OPTIONAL MATCH (id) <-[:part_of*1..]- (part)
WHERE NOT id IN collect(part) 
WITH collect(part.name) AS parts, id
RETURN id, parts


MATCH (id) -[r*0..]- (any)
WHERE NOT id:GECO AND labels(id) IN ['LS_Agent', 'LS_Agent_PP'] AND NOT type(r) IN ['part_of'] AND labels(any) IN ['LS_Agent', 'LS_Agent_PP']
RETURN id , any