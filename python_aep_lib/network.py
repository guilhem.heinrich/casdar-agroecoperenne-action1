import pydot

def drawAgent(agent):
    graph = pydot.Dot(graph_type='digraph')
    node_core = pydot.Node(agent['id']['name'], style="filled", fillcolor="green")
    graph.add_node(node_core)
    for sub in agent['subpart']:
        sub_node = pydot.Node(sub['name'], color="green")
        graph.add_node(sub_node)
        graph.add_edge(pydot.Edge(sub_node, node_core, label="part of"))
    return graph

