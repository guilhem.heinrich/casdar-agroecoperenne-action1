from SPARQLWrapper import SPARQLWrapper, JSON

sparql = SPARQLWrapper("http://138.102.159.59:4203/sparql")

def isChildOf(son_uri, father_uri):
    query = f"""
        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
        ASK {{
            <{son_uri}> skos:broader+ <{father_uri}>
        }}
    """
    print(query)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results['boolean']



# for result in results["results"]["bindings"]:
#     print('%s: %s' % (result["label"]["xml:lang"], result["label"]["value"]))
